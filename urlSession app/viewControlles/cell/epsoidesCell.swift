//
//  epsoidesCell.swift
//  ios-task
//
//  Created by mahroUS on 24/12/2564 BE.
//

import UIKit

class EpsoidesCell: UITableViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var movieTitleLB: UILabel!
    @IBOutlet weak var bannerImg: UIImageView!
    @IBOutlet weak var premieredLB: UILabel!
    @IBOutlet weak var rateLB: UILabel!
    @IBOutlet weak var runtimeLB: UILabel!
    @IBOutlet weak var linkLB: UILabel!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        mainViewUI()
        // Initialization code
    }
    func mainViewUI(){
        self.mainView.layer.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.4833503136)
        self.mainView.layer.cornerRadius = 5
        self.mainView.layer.shadowColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        self.mainView.layer.shadowRadius = 10
        self.mainView.layer.shadowOffset = CGSize(width: 4, height: 4)
        self.mainView.clipsToBounds = true
        self.mainView.layer.masksToBounds = false
        contentUI()

    }
    
    func contentUI(){
        self.bannerImg.layer.cornerRadius = 5
    }
    func configer(model: Show,medium: String?){
    
        let average = model.rating
        movieTitleLB.text = model.name ?? ""
        rateLB.text = "rate: " + "\((average?["average"] as? Double) ?? 0)"
        runtimeLB.text = "Runtime: " + "\(model.runtime ?? 0)"
        linkLB.text = "Link: " +  (model.url ?? "")
        premieredLB.text = (model.premiered ?? "")
        DispatchQueue.main.async {
            self.bannerImg.downloaded(from: medium ?? "")
        }
    }

}
