//
//  detailsMovieVc.swift
//  ios-task
//
//  Created by mahroUS on 25/12/2564 BE.
//

import UIKit

class DetailsMovieVc: UIViewController {

    @IBOutlet weak var bannarImg: UIImageView!
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var premieredLB: UILabel!
    @IBOutlet weak var rateLB: UILabel!
    @IBOutlet weak var runtimeLB: UILabel!
    @IBOutlet weak var linkLB: UILabel!
    @IBOutlet weak var descriptionsLB: UITextView!
    @IBOutlet weak var prevBtn: UIButton!
    @IBOutlet weak var linkBtn: UIButton!
    @IBOutlet weak var nextBtn: UIButton!
    
    var movieDetaila :Show?
    var link :String?
    var prevlink :String?
    var nextlink :String?
    override func viewDidLoad() {
        super.viewDidLoad()

        configUI()
        // Do any additional setup after loading the view.
    }

    @IBAction func linkActionBtn(_ sender: UIButton) {

        switch sender {
        case linkBtn:
       
            print("link is: ",self.link ?? "")
        case nextBtn:
            
            print("next link is: ",self.nextlink ?? "")

        default:
          
            print("prev link is: ",self.prevlink ?? "")
        }
    }
    
    
}
extension DetailsMovieVc {
   
    func configUI(){
        
        let average = movieDetaila?.rating

        titleLB.text = movieDetaila?.name ?? ""
        rateLB.text = "rate: " + "\((average?["average"] as? Double) ?? 0)"
        runtimeLB.text = "Runtime: " + "\(movieDetaila?.runtime ?? 0)"
        linkLB.text = "Link: " +  (movieDetaila?.url ?? "")
        premieredLB.text = (movieDetaila?.premiered ?? "")
        descriptionsLB.text = movieDetaila?.summary ?? ""
        removeHTMLTags()
        uplodeImg()
        getLinks()
    }
    
    func removeHTMLTags(){
       
        let str = movieDetaila?.summary?.replacingOccurrences(of: "<[^>]+>", with: "", options: .regularExpression, range: nil)

        descriptionsLB.text = str

    }
    func uplodeImg(){
      
        let medium = movieDetaila?.image?["medium"] as? String

        bannarImg.downloaded(from: medium ?? "")

    }
    
    func getLinks(){
        let selfLink = movieDetaila?._links?["self"] as? NSDictionary
        let prevLink = movieDetaila?._links?["previousepisode"] as? NSDictionary
        let nextLink = movieDetaila?._links?["nextepisode"] as? NSDictionary
        if selfLink != nil {

            self.linkBtn.isHidden = false
            self.link = selfLink?["href"] as? String
        }
        if prevLink != nil {

            self.prevBtn.isHidden = false
            self.prevlink = prevLink?["href"] as? String

        }
        if nextLink != nil {

            self.nextBtn.isHidden = false
            self.nextlink = nextLink?["href"] as? String

        }
    }

}
