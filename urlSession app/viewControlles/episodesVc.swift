//
//  episodesVc.swift
//  urlSession app
//
//  Created by mahroUS on 25/12/2564 BE.
//

import UIKit

class EpisodesVc: UIViewController {
    
    @IBOutlet weak var moviesTableView: UITableView!
    
    var moviewList = [Show]()
    
    let spinner = UIActivityIndicatorView(style: .medium)
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        configTableView(tableView: self.moviesTableView)
        getMovies()
        // Do any additional setup after loading the view.
    }
    func configTableView(tableView: UITableView?){
        tableView?.delegate = self
        tableView?.dataSource = self
    }
    
}
extension EpisodesVc : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return moviewList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! EpsoidesCell
       
        cell.mainViewUI()
        cell.configer(model: moviewList[indexPath.row], medium: moviewList[indexPath.row].image?["medium"] as? String)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let goToDetails = storyboard?.instantiateViewController(withIdentifier: "DetailsMovieVc") as! DetailsMovieVc
        
        goToDetails.movieDetaila = self.moviewList[indexPath.row]
        goToDetails.modalPresentationStyle = .fullScreen
        navigationController?.pushViewController(goToDetails, animated: true)
    }
    
}

extension EpisodesVc {
    func Parsing(json: NSArray){
        for item in json {
            
            let mainList = item as? NSDictionary
            let showList = mainList?["show"] as? NSDictionary
            let rating = showList?["rating"] as? NSDictionary
            let image = showList?["image"] as? NSDictionary
            let _links = showList?["_links"] as? NSDictionary
            self.moviewList.append(Show(id: showList?["id"] as? Int,url: showList?["url"] as? String, name: showList?["name"] as? String,runtime: showList?["runtime"] as? Int,premiered: showList?["premiered"] as? String,rating: rating,image: image,summary: showList?["summary"] as? String,_links: _links))
        }

    }
    func getMovies(){
        self.startTableAnimations(tableView: self.moviesTableView, spinner: self.spinner)
      
        guard let url = URL(string: "http://api.tvmaze.com/search/shows?q=old")  else {return}
        
        let session = URLSession.shared
        
        session.dataTask(with: url) { (data, response, error) in
            
            guard error == nil else {
                print("returned error")
                return
            }
            
            guard let content = data else {
                print("No data")
                return
            }

            if let json = try? JSONSerialization.jsonObject(with: content, options: []) as? NSArray {
                
                self.Parsing(json: json)
         
                DispatchQueue.main.async {
                  
                    self.stopTableAnimations(tableView: self.moviesTableView, spinner: self.spinner)
                }
                
                
            } else {
                
                print(error?.localizedDescription ?? "error")
                self.showConfirmationAlert(message: "Something goes wrong. Please try again.", okTitle: "Try again", cancelTitle: "OK", okClick: { [weak self] in
                    self?.moviewList.removeAll()
                    self?.getMovies()
                }) {}


            }
            
        }.resume()
        
    }
}

