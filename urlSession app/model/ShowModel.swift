//
//  ShowModel.swift
//  iosTask
//
//  Created by mahroUS on 24/12/2564 BE.
//

import Foundation

struct Show  {
    
    var id : Int?
    var url : String?
    var name : String?
    var type : String?
    var language : String?
    var status : String?
    var runtime : Int?
    var averageRuntime : Int?
    var premiered : String?
    var rating : NSDictionary?
    var image : NSDictionary?
    var summary : String?
    var _links : NSDictionary?
}
